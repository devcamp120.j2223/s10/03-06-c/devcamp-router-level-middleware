// Import bộ thư viện express
const express = require('express'); 

// Import Course Middleware
const { printCourseURLMiddleware } = require("../middlewares/courseMiddleware");

const router = express.Router();

router.get("/courses", printCourseURLMiddleware,  (request, response) => {
    response.json({
        message: "GET All Course"
    })
});

router.post("/courses", printCourseURLMiddleware, (request, response) => {
    response.json({
        message: "POST new Course"
    })
});

router.get("/courses/:courseId", printCourseURLMiddleware, (request, response) => {
    let courseId = request.params.courseId;

    response.json({
        message: "GET course ID = " + courseId
    })
});

router.put("/courses/:courseId", printCourseURLMiddleware, (request, response) => {
    let courseId = request.params.courseId;

    response.json({
        message: "PUT course ID = " + courseId
    })
});

router.delete("/courses/:courseId", printCourseURLMiddleware, (request, response) => {
    let courseId = request.params.courseId;

    response.json({
        message: "DELETE course ID = " + courseId
    })
});

// Export dữ liệu thành 1 module
module.exports = router;